package com.javastudy.deliveryservice.mvc.controller;

import com.javastudy.deliveryservice.mvc.model.Order;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Oleksii Miroshychenko
 */
@Controller
@RequestMapping(value = "/client/order")
public class OrderController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showForm() {
        return new ModelAndView("order/index", "ordermodel", new Order());
    }
    
    
    @RequestMapping(method = RequestMethod.POST)
    public String submit(@ModelAttribute("ordermodel") Order order,
            ModelMap model) {
        model.addAttribute("receiverName", order.getReceiverName());
        model.addAttribute("receiverAddress", order.getReceiverAddress());
        model.addAttribute("receiverEmail", order.getReceiverEmail());
        model.addAttribute("receiverPhone", order.getReceiverPhone());
        model.addAttribute("senderName", order.getSenderName());
        model.addAttribute("senderAddress", order.getSenderAddress());
        model.addAttribute("senderEmail", order.getSenderEmail());
        model.addAttribute("senderPhone", order.getSenderPhone());
        model.addAttribute("orderDate", order.getOrderDate());
        model.addAttribute("orderTime", order.getOrderTime());
        model.addAttribute("orderAdditionalInfo", order.getOrderAdditionalInfo());
        return "order/orderSuccess";
    }
}
