<%-- 
    Document   : order
    Created on : Jul 29, 2015, 10:45:40 AM
    Author     : skl1f
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        
        <h2>Receiver name: ${receiverName}</h2>
        <h2>Receiver address:  ${receiverAddress}</h2>
        <h2>Receiver email: ${receiverEmail}</h2>
        <h2>Receiver phone: ${receiverPhone}</h2>
        <h2>Sender name: ${senderName}</h2>
        <h2>Sender name:  ${senderAddress}</h2>
        <h2>Sender email: ${senderEmail}</h2>
        <h2>Sender phone: ${senderPhone}</h2>
        <h2>Order delivery date: ${orderDate}</h2>
        <h2>Order delivery time: ${orderTime}</h2>
        <h2>Order additional information: ${orderAdditionalInfo}</h2>

    </body>
</html>