<%-- 
    Document   : orderSuccess
    Created on : Jul 29, 2015, 12:09:11 PM
    Author     : skl1f
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="http://malsup.github.io/jquery.form.js"></script>
        <spring:url value="/resources/js/order.js" var="orderjs" />
        <script src="${orderjs}" type="text/javascript"><jsp:text/></script>
    </head>
    <body>
        <h3>Welcome, Enter The Employee Details</h3>
        <spring:url value="/client/order/" var="orderurl" />
        <form:form method="POST" action="${orderurl}" modelAttribute="ordermodel">
        <table>
                <tr>
                    <td><form:label path="receiverName">Receiver Name</form:label></td>
                    <td><form:input path="receiverName"/></td>
                </tr>
                <tr>
                    <td><form:label path="receiverAddress">Receiver Address</form:label></td>
                    <td><form:input path="receiverAddress"/></td>
                </tr>
                <tr>
                    <td><form:label path="receiverEmail">Receiver Email</form:label></td>
                    <td><form:input path="receiverEmail"/></td>
                </tr>
                <tr>
                    <td><form:label path="receiverPhone">Receiver Phone</form:label></td>
                    <td><form:input path="receiverPhone"/></td>
                </tr>
                                <tr>
                    <td><form:label path="senderName">Sender Name</form:label></td>
                    <td><form:input path="senderName"/></td>
                </tr>
                <tr>
                    <td><form:label path="senderAddress">Sender Address</form:label></td>
                    <td><form:input path="senderAddress"/></td>
                </tr>
                <tr>
                    <td><form:label path="senderEmail">Sender Email</form:label></td>
                    <td><form:input path="senderEmail"/></td>
                </tr>
                <tr>
                    <td><form:label path="senderPhone">Sender Phone</form:label></td>
                    <td><form:input path="senderPhone"/></td>
                </tr>
                <tr>
                    <td><form:label path="orderDate">Order Date</form:label></td>
                    <td><form:input path="orderDate"/></td>
                </tr>
                <tr>
                    <td><form:label path="orderTime">Order Time</form:label></td>
                    <td><form:input path="orderTime"/></td>
                </tr>
                <tr>
                    <td><form:label path="orderAdditionalInfo">Order Additional Info</form:label></td>
                    <td><form:input path="orderAdditionalInfo"/></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Submit"/></td>
                </tr>
            </table>
        </form:form>
    </body>
</html>
